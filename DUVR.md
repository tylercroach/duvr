Three websites that inspire me are http://www.oculus.com, https://vr.berkeley.edu/, and http://vrclub.ucsd.edu/.

I like the Oculus site because it is well designed. It looks modern and showcases the design of Oculus really well. Since VR is a modern technology, a website showing it off should have a modern design to it. The colors and fonts work really well to create this effect. Therefore, when creatinhg the DUVR site I should design it with a modern feel similar to the Oculus website.

The UC Berkeley VR Club website inspires me because it uses a video in as a background that shows the club in action. This is a really cool way to show people what the club is like. However, it uses one page to show all of its content which I don't really like. I think in order to have an effective website it should have multiple pages, especially for a club that is meant to attract new members.

The simplicity of the UC San Diego VR Club website is attractive to me because when a website is too cluttered, it can be hard for the user to find information. When looking at a VR club, most users are going to have questions in their minds that they want answers to, such as when the club meets and where. Therefore, the club's website should be simple enough so that this information can be found quickly and easily. I don't like the dull colors of the site and the generic font being used because it looks outdated. If the design of the website does not match the product or service it is promoting, it does not do its job very well, and people won't care about simplicity. Content and design go hand in hand.

About Me:
    My name is Tyler C. Roach, and I am an Interactive Digital Media major at Drexel with a passion for virtual and augmented reality technology. I work at an emerging technology company called BrickVR, where I develop software for VR/AR devices. I am passionate about making a difference in the world using my talents, and inspiring others to do the same.
    I came to Drexel because the school I was at did not offer me the academic major that I wanted to study. In the future, I want to develop and design user interfaces that take advantage of virtual and augmented reality to make it easier and more natural to use technology on a daily basis. Interactive Digital Media teaches me the fundamental techniques in designing these interfaces as well as giving me a general sense of how things should be designed. I am glad I came here because I am now on my way to doing what I have always wanted to do in life.
    Along with having a passion for VR/AR, I also enjoy playing tennis and singing. I started playing tennis when I was a junior in high school and I have been playing it ever since. My grandmother is actually the person who got me interested in the sport, as she played it much of her adult life. I like it because it keeps you moving and you are always in the action, unlike baseball where you have to wait for your turn to play.
    At Drexel, I am able to do what I love to do. I am a member of DUVR, the Drexel VR Club. This club meets weekly and provides demos of VR/AR hardware to people who have never used the technology before. It also allows developers like me to show off their experiences to a like-minded audience. I have chosen to create the DUVR website because it deserves to have one so that it will get more recognition and people will have a better idea of what it is all about.
    
My Resume: https://www.visualcv.com/app/#/cvs/1276106
My contact information: Tyler C. Roach
                        3400 Lancaster Avenue Apt. 701
                        Philadelphia, PA 19104
                    
                        2677985912
                        tcr45@drexel.edu
